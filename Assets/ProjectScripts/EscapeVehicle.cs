﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeVehicle : MonoBehaviour
{
    [Header("Audio")]
    public AudioSource carStartAudio;
    [Header("Lights")]
    public Renderer lightsRenderer;
    public Material lightOn;
    public Material lightsOff;
    public Light leftHighLight;
    public Light rightHighLight;
    public Light leftPointight;
    public Light rightPointLight;
    [Space]
    public GameManager gameManager;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            carStartAudio.Play(0);
            lightsRenderer.material = lightOn;
            leftHighLight.intensity = 1;
            rightHighLight.intensity = 1;

            leftPointight.intensity = 30;
            rightPointLight.intensity = 30;
        }
    }

    public void ReturnToMainMenu()
    {
        StartCoroutine(IEReturnToMainMenu());
    }

    IEnumerator IEReturnToMainMenu()
    {
        yield return new WaitForSeconds(5);
        gameManager.LoadLevel(0);
    }
}

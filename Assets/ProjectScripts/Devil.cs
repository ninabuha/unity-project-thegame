﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Devil : MonoBehaviour
{

    public Animator transition;
    public GameObject player;
    private float devilSpeed = 4f;

    private Vector3 currentPosition;
    private Vector3 playerPosition;
    
    private bool attack = true;
    public AudioSource devilAudio;
    public AudioSource attackAudio;

    private void Start()
    {
        RotateTowardsPlayer();
        StartCoroutine(PlayAnimation());
    }

    // Update is called once per frame
    void Update()
    {
        currentPosition = transform.position;
        // follow player
        if (!attack) {
            MoveTowardsPlayer();
        }
        if (transform.position - currentPosition != Vector3.zero)
        {
            RotateTowardsPlayer();
        }
    }

    void MoveTowardsPlayer()
    {
        if (!devilAudio.isPlaying)
        {
            StartCoroutine(PlayAudio());
        }
        playerPosition = player.transform.position;
        playerPosition.y = 0f;
        transform.position = Vector3.MoveTowards(transform.position, playerPosition, devilSpeed * Time.deltaTime);
    }

    void RotateTowardsPlayer()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - currentPosition);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            attack = true;
            FPSControlls.DecreseHealth();
            StartCoroutine(PlayAnimation());
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && !attack)
        {
            attack = true;
            FPSControlls.DecreseHealth();
            StartCoroutine(PlayAnimation());
        }
    }

    private IEnumerator PlayAnimation()
    {
        // play animation
        transition.SetTrigger("Attack");
        yield return new WaitForSeconds(0.5f);
        attackAudio.Play();
        // wait for animation to finish
        yield return new WaitForSeconds(1.5f);
        attack = false;
    }

    IEnumerator PlayAudio()
    {
        devilAudio.Play(0);
        yield return new WaitWhile(() => devilAudio.isPlaying);
        devilAudio.Stop();
    }
}

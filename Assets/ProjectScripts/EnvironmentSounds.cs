﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSounds : MonoBehaviour
{
    public AudioSource owlAudio;
    private double timeCounter = 0f;

    // Update is called once per frame
    void Update()
    {
        if (timeCounter > 20f)
        {
            if (Random.Range(1, 100) > 80 && !owlAudio.isPlaying)
            {
                StartCoroutine(PlayAudio());
                timeCounter = 0.0f;
            }
        }
        timeCounter += Time.deltaTime;
    }

    IEnumerator PlayAudio()
    {
        owlAudio.Play(0);
        yield return new WaitWhile(() => owlAudio.isPlaying);
        owlAudio.Stop();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Animator transition;
    public float transitionTime = 1f;

    public void StartGame()
    {
        Debug.Log("Start Game");
        LoadLevel(1);
    }

    public void QuitGame()
    {
        Debug.Log("Good Bye!");
        Application.Quit();
    }

    public void LoadLevel(int levelIndex)
    {
        StartCoroutine(IELoadLevel(levelIndex));
    }

    private IEnumerator IELoadLevel(int levelIndex)
    {
        // play animation
        transition.SetTrigger("StartTrigger");
        // wait for animation to finish
        yield return new WaitForSeconds(transitionTime);
        // load scene
        SceneManager.LoadScene(levelIndex);
    }

    public void LoadCredits()
    {
        StartCoroutine(IELoadLevelByName("Credits"));
    }

    public void LoadMainMenu()
    {
        StartCoroutine(IELoadLevelByName("Menu"));
    }

    public void LoadSettings()
    {
        StartCoroutine(IELoadLevelByName("Settings"));
    }

    private IEnumerator IELoadLevelByName(string levelName)
    {
        // play animation
        transition.SetTrigger("StartTrigger");
        // wait for animation to finish
        yield return new WaitForSeconds(transitionTime);
        // load scene
        SceneManager.LoadScene(levelName);
    }
}

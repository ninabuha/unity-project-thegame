﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIMessage : MonoBehaviour
{
    public static Animator transition;
    public Animator publicTransition;
    private static TMP_Text textMessage;

    // Start is called before the first frame update
    void Start()
    {
        textMessage = GameObject.FindGameObjectWithTag("UIMessage").GetComponent<TMP_Text>();
        transition = publicTransition;
    }

    // Update is called once per frame
    public static void ShowUIMessage(string message)
    {
        textMessage.text = message;
        transition.SetTrigger("ShowUIMessage");
    }
}

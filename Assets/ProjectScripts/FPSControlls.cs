﻿using System.Collections;
using UnityEngine;

public class FPSControlls : MonoBehaviour
{

    public GameManager gameManager;
    [Header("Audio")]
    private AudioSource audioData;
    public AudioSource branchCrackAudio;
    public AudioSource treeFallAudio;
    private bool treeFell = false;
    public AudioSource animalKilledAudio;
    private bool animalKilled = false;
    public AudioSource runAudio;
    private bool run = false;
    public AudioSource killKillKillAudio;
    private bool killKillKill = false;
    public AudioSource woodsAudio;
    private bool playingWoodsAudio = false;
    public GameObject monster;
    public GameObject devil;
    public AudioSource woodCrackAudio;
    private bool woodCrack = false;
    public AudioSource spiderJumpscareAudio;
    private bool spiderJumpscare = false;
    public GameObject spider;

    [Header("Player")]
    public CharacterController characterController;
    public Light flashlight;
    public GameObject flashlightOnGround;
    public Light woodenHouseLight;
    public GameObject key;
    private float playerSpeed = 10f;
    private float jumpHeight = 3f;
    [Space]
    private float gravity = -29.81f;
    private Vector3 velocity;
    public Transform groundCheck;
    private float groundDistance = 0.4f;
    public LayerMask groundMask;
    private bool isGrounded;

    Vector3 motion;
    Vector3 lastPosition;

    private bool flashlightPickedUp = false;
    private bool keyPickedUp = false;
    public GameObject gate;

    private float x, z, speed, dist;

    private static int health = 3;
    public EscapeVehicle vehicle;


    void Start()
    {
        audioData = GetComponent<AudioSource>();
        Cursor.visible = false;
        health = 3;
    }

    void Update()
    {
        Move();
        if (health == 0)
        {
            gameManager.LoadLevel(0);
        }
    }

    // player movement
    void Move()
    {
        dist = Vector3.Distance(transform.position, lastPosition);
        lastPosition = transform.position;
        speed = dist / Time.deltaTime;

        isGrounded = IsGrounded();
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");
        motion = transform.right * x + transform.forward * z;
        characterController.Move(motion * playerSpeed * Time.deltaTime);

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
        
        if (speed > 0 && !audioData.isPlaying && isGrounded)
        {
            StartCoroutine(PlayAudio());
        }

        // adding gravity
        velocity.y += gravity * Time.deltaTime;
        characterController.Move(velocity * Time.deltaTime);
    }

    // check if player is on the ground
    bool IsGrounded()
    {
        return Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
    }

    IEnumerator PlayAudio()
    {
        audioData.Play(0);
        yield return new WaitWhile(() => audioData.isPlaying);
        audioData.Stop();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FlashLightTrigger")
        {
            UIMessage.ShowUIMessage("Press F to pick up The Flashlight");
        }
        if (other.tag == "KeyTrigger")
        {
            UIMessage.ShowUIMessage("Press F to pick up The Key");
        }
        if (other.tag == "BranchTrigger")
        {
            branchCrackAudio.Play(0);
        }
        if (other.tag == "TreeFallTrigger" && treeFell == false)
        {
            treeFell = true;
            treeFallAudio.Play(0);
        }
        if (other.tag == "AnimalKilledTrigger" && animalKilled == false)
        {
            animalKilled = true;
            animalKilledAudio.Play(0);
            run = true;
            runAudio.Play(0);
        }
        if (other.tag == "KillKillKillTrigger" && killKillKill == false)
        {
            killKillKill = true;
            killKillKillAudio.Play(0);
        }
        if (other.tag == "NoteTrigger")
        {
            woodenHouseLight.intensity = 1;
        }
        if (other.tag == "WoodCrackTrigger" && woodCrack == false)
        {
            woodCrack = true;
            woodCrackAudio.Play(0);
        }
        if (other.tag == "SpiderTrigger" && spiderJumpscare == false)
        {
            spider.SetActive(true);
            spiderJumpscare = true;
            spiderJumpscareAudio.Play();
        }
        if (other.tag == "Escape")
        {
            vehicle.ReturnToMainMenu();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "FlashLightTrigger" && Input.GetKey(KeyCode.F) && !flashlightPickedUp)
        {
            flashlight.intensity = 0.5f;
            flashlightPickedUp = true;
            Destroy(flashlightOnGround);

            UIMessage.ShowUIMessage("See the Forest?");
        }

        if (other.tag == "KeyTrigger" && Input.GetKey(KeyCode.F) && !keyPickedUp)
        {
            keyPickedUp = true;
            gate.SetActive(false);
            Destroy(key);

            //monster.SetActive(true);
            devil.SetActive(true);
            UIMessage.ShowUIMessage("RUN!");
        }

        if (other.tag == "WoodsTrigger" && !playingWoodsAudio)
        {
            woodsAudio.volume = 1;
            woodsAudio.Play(0);
            playingWoodsAudio = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "WoodsTrigger" && !playingWoodsAudio)
        {
            PauseWoodsAudio();
        }
    }

    void PauseWoodsAudio()
    {
        woodsAudio.volume = Mathf.Lerp(0f, 0.2f, Time.time);
        playingWoodsAudio = false;
    }

    public static void DecreseHealth()
    {
        health--;
        if (health == 0)
        {
            UIMessage.ShowUIMessage("YOU HAVE DIED!");
        }
    }
}

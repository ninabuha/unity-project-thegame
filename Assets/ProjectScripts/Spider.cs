﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour
{
    private float spiderSpeed = 25f;
    public GameObject destination;
    Vector3 newPosition;

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, destination.transform.position, spiderSpeed * Time.deltaTime);

        if (Vector3.Distance(destination.transform.position, transform.position) < 10)
        {
            Destroy(gameObject);
            Destroy(destination);
        }
    }
}

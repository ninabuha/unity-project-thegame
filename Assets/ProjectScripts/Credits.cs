﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    public GameManager gameManager;

    public void ReturnToMainMenu()
    {
        gameManager.LoadLevel(0);
    }
}

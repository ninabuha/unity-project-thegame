﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRoom : MonoBehaviour
{
    public GameObject roomLight;
    public GameObject roomPointLight;
    public GameObject mainCamera;
    public GameObject theGameText;
    int randomNumber;
    public Material lightMaterial;
    public Material noLightMaterial;

    public GameObject babySpotLight;
    public GameObject babySpotLightRed;

    private int counter = 0;

    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void FixedUpdate()
    {
        counter++;
        randomNumber = Random.Range(1, 20);
        if (randomNumber == 5)
        {
            roomPointLight.GetComponent<Light>().intensity = 0.5f; 
            roomLight.GetComponent<MeshRenderer>().material = noLightMaterial;
            theGameText.SetActive(false);

            babySpotLight.GetComponent<Light>().intensity = 2f;
            babySpotLightRed.GetComponent<Light>().intensity = 1f;
        }
        else
        {
            roomPointLight.GetComponent<Light>().intensity = 0;
            roomLight.GetComponent<MeshRenderer>().material = lightMaterial;
            theGameText.SetActive(true);

            babySpotLight.GetComponent<Light>().intensity = 0;
            babySpotLightRed.GetComponent<Light>().intensity = 0;
        }

        if (counter > 100)
        {
            mainCamera.transform.Rotate(Vector3.down, Time.deltaTime * 10f);
        }


        // first idea with the camera 

        /*if (counter%100 == 0)
        {
            Vector3 tempRoomPos = mainCamera.transform.position;
            mainCamera.transform.rotation = Quaternion.Euler(tempRoomPos.x, tempRoomPos.y, 10);
        }
        else if(counter%105 == 0)
        {
            Vector3 tempRoomPos = mainCamera.transform.position;
            mainCamera.transform.rotation = Quaternion.Euler(tempRoomPos.x, tempRoomPos.y, -10);
        }
        else
        {
            Vector3 tempRoomPos = mainCamera.transform.position;
            mainCamera.transform.rotation = Quaternion.Euler(tempRoomPos.x, tempRoomPos.y, 0);
        }*/
    }
}

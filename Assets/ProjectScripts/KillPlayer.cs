﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    public Light babySpotLight;
    private GameObject player;
    private float babyDollSpeed = 2.5f;
    private AudioSource audioData;

    private Vector3 currentPosition;
    private Vector3 playerPosition;

    private double timeCounter = 0f;

    public GameObject newBabyDoll;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        currentPosition = transform.position;
        // follow player
        MoveTowardsPlayer();
        if (transform.position - currentPosition != Vector3.zero)
        {
            RotateTowardsPlayer();
        }
        CheckPlayerDistance();
    }

    void MoveTowardsPlayer()
    {
        playerPosition = player.transform.position;
        playerPosition.y = 2.6f;
        transform.position = Vector3.MoveTowards(transform.position, playerPosition, babyDollSpeed * Time.deltaTime);
    }

    void RotateTowardsPlayer()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - currentPosition);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == player)
        {
            // creating new BabyDoll
            CreateBabyDoll(gameObject, newBabyDoll.transform.position, transform.rotation);
            babyDollSpeed = 4f;
            DestroyObject();
            Debug.Log("Object Destroyed");
        } else
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }

    void DestroyObject()
    {
        Destroy(gameObject);
    }

    public static void CreateBabyDoll(GameObject gameObject, Vector3 position, Quaternion rotation)
    {
        Instantiate(gameObject, position, rotation);
    }

    void CheckPlayerDistance()
    {
        if (Vector3.Distance(player.transform.position, transform.position) < 10)
        {
            BabyLightFlicker();
            if (!audioData.isPlaying)
            {
                PlayAudio();
            }
        } else
        {
            babySpotLight.intensity = 0;
            PauseAudio();
        }
    }

    void BabyLightFlicker()
    {
        babySpotLight.intensity = 0;
        if (timeCounter > 1f)
        {
            if (Random.Range(0, 10) > 7)
            {
                babySpotLight.intensity = 1;
            }
        }
        timeCounter += Time.deltaTime;
    }

    void PlayAudio()
    {
        audioData.volume = 1;
        audioData.Play(0);
    }

    void PauseAudio()
    {
        audioData.volume = Mathf.Lerp(0f, 0.1f, Time.time);
    }
}

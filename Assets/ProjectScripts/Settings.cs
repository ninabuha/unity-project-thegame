﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Slider mouseSensitivity;
    public Slider volumeSlider;
    public TMP_Dropdown qualityLevel;

    private void Start()
    {
        mouseSensitivity.value = MouseLook.GetMouseSensitivity();
        volumeSlider.value = AudioListener.volume;
        qualityLevel.value = QualitySettings.GetQualityLevel();
    }

    public void SetMouseSensitivity(float sensitivity)
    {
        MouseLook.SetMouseSensitivity(sensitivity);
    }

    public void SetVolume(float volume)
    {
        AudioListener.volume = volume;
        // ?
        // audioMixer.SetFloat("Volume", Mathf.Log10(volume) * 20);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
